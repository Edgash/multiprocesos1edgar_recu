import java.io.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

public class Ejercicio3 {
    public static void main(String[] args) throws IOException {
        //esta es la ruta del jar
        String command = "java -jar out/artifacts/Minusculas_jar/Minusculas.jar";
        List<String> argList = new ArrayList<>(Arrays.asList(command.split(" ")));
        ProcessBuilder pb = new ProcessBuilder(argList);

        try{
            Process process = pb.start();
            OutputStream os = process.getOutputStream();
            OutputStreamWriter osw = new OutputStreamWriter(os);
            BufferedWriter bw = new BufferedWriter(osw);
            //un scanner que pide una cadena de texto
            System.out.println("Escribe algo");
            Scanner procesoSC = new Scanner(process.getInputStream());
            Scanner sc = new Scanner(System.in);
            String linea = sc.nextLine().toLowerCase();
            //esto compara que lo que hayas escrito no sea la palabra 'finalizar'
            while(!linea.equals("finalizar")){
                //estas líneas guardan la linea escrita
                bw.write(linea);
                bw.newLine();
                bw.flush();
                //muestra en pantalla el texto escrito pero en minusculas
                System.out.println(procesoSC.nextLine());
                linea = sc.nextLine().toLowerCase();
            }
            //detecta el error de fallo del proceso
        } catch (IOException e) {
            e.printStackTrace();
            System.out.println("Fallo del proceso secundario");
        }
    }
}
