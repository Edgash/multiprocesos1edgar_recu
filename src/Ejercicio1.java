import java.awt.*;
import java.io.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.TimeUnit;

//este es el proyecto que te comenté el otro día que me pusiste que era copiado pero no lo es, por lo que el código es
// basicamente el mismo (menos un par de líneas innecesarias que he borrado)
public class Ejercicio1 {
    public static void main(String[] args) throws IOException {

        //aquí definimos los parametros y le asignamos el comando que hay en 'args' que es 'ls -la'
        List<String> argList = new ArrayList<>(Arrays.asList());
        ProcessBuilder hijo = new ProcessBuilder(args);
        Process padre = hijo.start();

        //aquí le ponemos el tiempo que el padre está esperando y si falla lo notifica
        try {
            if(!padre.waitFor(2, TimeUnit.SECONDS)){
                throw new InterruptedException();
            }
            //aquí ponemos el mensaje que da si el tiempo se agota y el System.exit finaliza el programa
        } catch (InterruptedException e) {
            System.out.println("Tiempo de espera agotado");
            System.exit(-1);
        }

        InputStream is = padre.getInputStream();
        InputStreamReader isr = new InputStreamReader(is);
        BufferedReader br = new BufferedReader(isr);
        //aquí está la ruta de donde guardamos el fichero
        BufferedWriter bw = new BufferedWriter(new FileWriter("output.txt"));
        //dos variables strings creadas para poder guardar la información en un fichero
        String line;
        String texto = "";

            while((line = br.readLine()) != null) {
                try {
                    //aquí muestra el contenido del line (que es el 'ls -la')
                    System.out.println(line);
                    texto += line + "\n";
                } catch (Exception e){
                    //aquí ponemos el mensaje de error si el hijo falla
                System.out.println("El hijo ha fallado");
                }
            }
            //escribe el contenido de texto en el fichero
            bw.write(texto);
            bw.close();

    }
}
