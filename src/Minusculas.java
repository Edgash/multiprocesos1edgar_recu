import java.util.Scanner;

public class Minusculas {
    public static void main(String[] args) {
        //un scanner que pide una cadena de texto
        System.out.println("Escribe algo");
        Scanner sc = new Scanner(System.in);
        //detecta las mayusculas
        String i = sc.nextLine().toLowerCase();
        ////pilla si la cadena de texto introducida es la palabra 'finalizar', si no lo es entra en el bucle
        while (!i.equals("finalizar")){
            //aquí imprime el texto que tu has introducido pero sin mayusculas
            System.out.println(i);
            i = sc.nextLine().toLowerCase();
        }
    }
}
